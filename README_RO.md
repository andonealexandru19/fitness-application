Aplicatie de # Fitness

## Conceptul
Aceasta aplicatie trebuie sa genereze un meniu sanatos si un program de antrenament, conducand astfel la obtinerea unor rezultate
mai bune in ceea ce priveste un stil de viata mai sanatos si un trup bine cladit. Am ales dintr-o mare varietate de exercitii si
preparate culinare pentru a oferi utilizatorului un efect satisfacator. Dupa ce am discutat cu un nutritionist, am reusit sa concepem
un algoritm care sorteaza mesele pentru o saptamana intreaga, astfel incat sa creeze nu o dieta sau o cura de slabire, ci un meniu echilibrat.
Cu toate acestea, daca sunteti interesat sa obtineti o masa pentru o singura zi, dar nu un meniu complet, incercati generatorul de o zi.
Luati-va exercitiile preferate si lucrati doar partile pe care le doriti prin utilizarea generatorului de exercitii, astfel avand
parte de un timp de calitate si rezultate mai rapide.

## De ce?
Aceasta aplicatie este proiectul nostru pentru un concurs numit "Infoeducatie", avand ca scop educarea utilizatorilor in alegerea
unui stil de viata extraordinar. De asemenea, merita mentionat faptul ca obezitatea nu a mai fost niciodata o problema atat de mare
ca pana astazi, cand viata sedentara si obiceiurile proaste in ceea ce priveste rutina alimentara ghideaza viata cotidiana.
Din acest motiv, am decis sa cream un site web dedicat imbunatatirii vietii utilizatorului prin adaugarea de exercitii fizice si
mese sanatoase in programul personal. Scopul nostru este de a avea o societate mai responsabila din aceasta perspectiva.
# Fitness app

## The concept
This application is supposed to generate a healthy meal and a workout routine, therefore leading to better results in achieving a
healthier lifestyle and a well structured body shape. We chose from a large variety of exercises and meals in order to give the user
a satisfying result. After talking to a nutritionist, we were able to come up with an algorithm that sorts meals for a whole week in
such a manner that it would not create a slimming diet, but rather a balanced way of eating. However, if you are interested in
getting a one day meal but not a full week one, check out the one day generator.
Pick up your favorite exercises and workout only the parts that you desire by using the exercises generator for spending quality time
and getting results faster.

## Why?
This app is our entry in a contest called "Infoeducatie", having the purpose of educating the users into picking a great life style.
It is also worth mentioning that such obesity levels have never been reported in history that today, when sedentary life and bad habits
in terms of food rule one's routine. This is why we decided to create a website dedicated to improving the user's schedule by adding 
physical exercises and healthy meals. Our goal is to have a more responsible society from this point of view.
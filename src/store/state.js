import breakfast from '../data/breakfast'
import beforeLunch from '../data/before_lunch'
import lunch from '../data/lunch'
import afterLunch from '../data/after_lunch'
import dinner from '../data/dinner'
import breakfastRO from '../data/breakfast_RO'
import beforeLunchRO from '../data/before_lunch_RO'
import lunchRO from '../data/lunch_RO'
import afterLunchRO from '../data/after_lunch_RO'
import dinnerRO from '../data/dinner_RO'
import sport from '../data/sport_EN'

export default {
  breakfast,
  beforeLunch,
  lunch,
  afterLunch,
  dinner,
  sport,
  breakfastRO,
  beforeLunchRO,
  lunchRO,
  afterLunchRO,
  dinnerRO
}

import _ from 'lodash'

export default {
  getBreakfast: (state) => {
    return _.shuffle(state.breakfast)
  },
  getBeforeLunch: (state) => {
    return _.shuffle(state.beforeLunch)
  },
  getLunch: (state) => {
    let array = _.shuffle(state.lunch)
    return _.uniqBy(array, 'type')
  },
  getAfterLunch: (state) => {
    return _.shuffle(state.afterLunch)
  },
  getDinner: (state) => {
    let array = _.shuffle(state.dinner)
    return _.uniqBy(array, 'type')
  },
  getBreakfastRO: (state) => {
    return _.shuffle(state.breakfastRO)
  },
  getBeforeLunchRO: (state) => {
    return _.shuffle(state.beforeLunchRO)
  },
  getLunchRO: (state) => {
    let array = _.shuffle(state.lunchRO)
    return _.uniqBy(array, 'type')
  },
  getAfterLunchRO: (state) => {
    return _.shuffle(state.afterLunchRO)
  },
  getDinnerRO: (state) => {
    let array = _.shuffle(state.dinnerRO)
    return _.uniqBy(array, 'type')
  },
  getUpper: (state) => {
    let array = _.filter(state.sport, { 'zone': 'Upper body' })
    array = _.shuffle(array)
    return array
  },
  getCore: (state) => {
    let array = _.filter(state.sport, { 'zone': 'Core Strength' })
    array = _.shuffle(array)
    return array
  },
  getLower: (state) => {
    let array = _.filter(state.sport, { 'zone': 'Lower Body' })
    array = _.shuffle(array)
    return array
  },
  getStretches: (state) => {
    let array = _.filter(state.sport, { 'zone': 'Stretches' })
    array = _.shuffle(array)
    return array
  },
  getCardio: (state) => {
    let array = _.filter(state.sport, { 'zone': 'Cardio' })
    array = _.shuffle(array)
    return array
  },
  getYoga: (state) => {
    let array = _.filter(state.sport, { 'zone': 'Yoga Poses' })
    array = _.shuffle(array)
    return array
  }
}

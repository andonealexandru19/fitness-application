export default [
	{
		name: 'Fruit',
		quantity: '1 fruit',
		hour: '17:00'
	},
	{
		name: 'Cereal bar',
		quantity: '1 bar',
		hour: '17:00'
	},
	{
		name: 'Almonds and fruit',
		quantity: '8 almonds, 1 fruit',
		hour: '17:00'
	},
	{
		name: 'Fruit yoghurt',
		quantity: '150 g fruit yoghurt, 0% fat',
		hour: '17:00'
	},
	{
		name: 'Frozen yoghurt with fruits',
		quantity: '1 medium C frozen yoghurt and fruits',
		hour: '17:00'
	}
]
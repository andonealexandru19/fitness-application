export default [
	{
		name: 'Tuna salad',
		quantity: 'salad, 1 small tuna can, 1 tsp light mayonnaise, (optional) 1/2 green apple',
		hour: '20:30',
		type: '1'
	},
	{
		name: 'Avocado salad',
		quantity: 'salad, 1/2 avocado',
		hour: '20:30',
		type: '1'
	},
	{
		name: 'Omlet and vegetables',
		quantity: '2 eggs, various vegetables, 1 slice of low fat cheese (12%)',
		hour: '20:30',
		type: '2'
	},
	{
		name: 'Feta Salad',
		quantity: 'salad, 1 medium piece of feta',
		hour: '20:30',
		type: '3'
	},
	{
		name: 'Chicken breast salad',
		quantity: '1 chicken breast fillet, salad',
		hour: '20:30',
		type: '3'
	},
	{
		name: 'Beetroot salad and eggs',
		quantity: '2 boiled eggs, 1 beetroot, salad',
		hour: '20:30',
		type: '4'
	},
	{
		name: 'Chicken breast and salad',
		quantity: '1 chicken breast fillet, salad',
		hour: '20:30',
		type: '5'
	},
	{
		name: 'Chicken kebab',
		quantity: '1 pita, 2 tbsp yoghurt, 2 chicken kebab sticks',
		hour: '20:30',
		type: '6'
	},
	{
		name: 'Ham and cheese',
		quantity: '2 slices of whole wheat bread, 1 slice of ham, 1 slice of low fat cheese (12%)',
		hour: '20:30',
		type: '7'
	}
]
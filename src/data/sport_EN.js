export default [
	{
		zone: 'Upper body',
		name: 'Alternating Push-Up Plank',
		image: 'Alternating Push-Up Plank.jpg'
	},
	{
		zone: 'Upper body',
		name: 'Chest expander',
		image: 'Chest expander.jpg'
	},
	{
		zone: 'Upper body',
		name: 'Diamond Push-Ups',
		image: 'Diamond Push-Ups.jpg'
	},
	{
		zone: 'Upper body',
		name: 'Dive Bomber Push-Ups',
		image: 'Dive Bomber Push-Ups.jpg'
	},
	{
		zone: 'Upper body',
		name: 'Jumping Jacks',
		image: 'Jumping Jacks.jpg'
	},
	{
		zone: 'Upper body',
		name: 'Lying Triceps Lifts',
		image: 'Lying Triceps Lifts.jpg'
	},
	{
		zone: 'Upper body',
		name: 'One Arm Side Push-up',
		image: 'One Arm Side Push-up.jpg'
	},
	{
		zone: 'Upper body',
		name: 'Overhead Arm Clap',
		image: 'Overhead Arm Clap.jpg'
	},
	{
		zone: 'Upper body',
		name: 'Overhead Press',
		image: 'Overhead Press.jpg'
	},
	{
		zone: 'Upper body',
		name: 'Power Circles',
		image: 'Power Circles.jpg'
	},
	{
		zone: 'Core Strength',
		name: 'Abdominal Crunch',
		image: 'Abdominal Crunch.jpg'
	},
	{
		zone: 'Core Strength',
		name: 'Bent Leg Twist',
		image: 'Bent Leg Twist.jpg'
	},
	{
		zone: 'Core Strength',
		name: 'Burpees',
		image: 'Burpees.jpg'
	},
	{
		zone: 'Core Strength',
		name: 'Elevated Crunches',
		image: 'Elevated Crunches.jpg'
	},
	{
		zone: 'Core Strength',
		name: 'Good Mornings',
		image: 'Good Mornings.jpg'
	},
	{
		zone: 'Core Strength',
		name: 'In and Out Abs',
		image: 'In and Out Abs.jpg'
	},
	{
		zone: 'Core Strength',
		name: 'Inch Worms',
		image: 'Inch Worms.jpg'
	},
	{
		zone: 'Core Strength',
		name: 'Leg Lifts',
		image: 'Leg Lifts.jpg'
	},
	{
		zone: 'Core Strength',
		name: 'Leg Spreaders',
		image: 'Leg Spreaders.jpg'
	},
	{
		zone: 'Core Strength',
		name: 'Mason Twist',
		image: 'Mason Twist.jpg'
	},
	{
		zone: 'Core Strength',
		name: 'Plank',
		image: 'Plank.jpg'
	},
	{
		zone: 'Core Strength',
		name: 'Side Bridge',
		image: 'Side Bridge.jpg'
	},
	{
		zone: 'Lower Body',
		name: 'Calf Raises',
		image: 'Calf Raises.jpg'
	},
	{
		zone: 'Lower Body',
		name: 'Forward Lunges',
		image: 'Forward Lunges.jpg'
	},
	{
		zone: 'Lower Body',
		name: 'Frog Jumps',
		image: 'Frog Jumps.jpg'
	},
	{
		zone: 'Lower Body',
		name: 'Front Kicks',
		image: 'Front Kicks.jpg'
	},
	{
		zone: 'Lower Body',
		name: 'High Jumper',
		image: 'High Jumper.jpg'
	},
	{
		zone: 'Lower Body',
		name: 'Jump Squats',
		image: 'Jump Squats.jpg'
	},
	{
		zone: 'Lower Body',
		name: 'Montain Climber',
		image: 'Montain Climber.jpg'
	},
	{
		zone: 'Lower Body',
		name: 'Rear Lunges',
		image: 'Rear Lunges.jpg'
	},
	{
		zone: 'Lower Body',
		name: 'Running in Place',
		image: 'Running in Place.jpg'
	},
	{
		zone: 'Lower Body',
		name: 'Side Leg Lifts',
		image: 'Side Leg Lifts.jpg'
	},
	{
		zone: 'Lower Body',
		name: 'Side to Side Knee Lifts',
		image: 'Side to Side Knee Lifts.jpg'
	},
	{
		zone: 'Stretches',
		name: 'Abdominal Stretch',
		image: 'Abdominal Stretch.jpg'
	},
	{
		zone: 'Stretches',
		name: 'Ankle on the Knee',
		image: 'Ankle on the Knee.jpg'
	},
	{
		zone: 'Stretches',
		name: 'Arm and Shoulder Stretch',
		image: 'Arm and Shoulder Stretch.jpg'
	},
	{
		zone: 'Stretches',
		name: 'Arm Circles',
		image: 'Arm Circles.jpg'
	},
	{
		zone: 'Stretches',
		name: 'Bend and Reach',
		image: 'Bend and Reach.jpg'
	},
	{
		zone: 'Stretches',
		name: 'Calf Stretch',
		image: 'Calf Stretch.jpg'
	},
	{
		zone: 'Stretches',
		name: 'Chest Stretch',
		image: 'Chest Stretch.jpg'
	},
	{
		zone: 'Stretches',
		name: 'Hamstring Stretch Standing',
		image: 'Hamstring Stretch Standing.jpg'
	},
	{
		zone: 'Stretches',
		name: 'Knee to Chest Stretch',
		image: 'Knee to Chest Stretch.jpg'
	},
	{
		zone: 'Cardio',
		name: 'Butt Kickers',
		image: 'Butt Kickers.jpg'
	},
	{
		zone: 'Cardio',
		name: 'Fast Feet',
		image: 'Fast Feet.jpg'
	},
	{
		zone: 'Cardio',
		name: 'High Knees',
		image: 'High Knees.jpg'
	},
	{
		zone: 'Cardio',
		name: 'Jumping Planks',
		image: 'Jumping Planks.jpg'
	},
	{
		zone: 'Cardio',
		name: 'Lunge Jumps',
		image: 'Lunge Jumps.jpg'
	},
	{
		zone: 'Cardio',
		name: 'Pivoting Upper Cuts',
		image: 'Pivoting Upper Cuts.jpg'
	},
	{
		zone: 'Cardio',
		name: 'Power Jump',
		image: 'Power Jump.jpg'
	},
	{
		zone: 'Cardio',
		name: 'Power Skip',
		image: 'Power Skip.jpg'
	},
	{
		zone: 'Cardio',
		name: 'Shoulder Tap Push-Ups',
		image: 'Shoulder Tap Push-Ups.jpg'
	},
	{
		zone: 'Cardio',
		name: 'Side Hops',
		image: 'Side Hops.jpg'
	},
	{
		zone: 'Cardio',
		name: 'Single Lateral Hops',
		image: 'Single Lateral Hops.jpg'
	},
	{
		zone: 'Yoga Poses',
		name: 'Chair Pose',
		image: 'Chair Pose.jpg'
	},
	{
		zone: 'Yoga Poses',
		name: 'Cobra Pose',
		image: 'Cobra Pose.jpg'
	},
	{
		zone: 'Yoga Poses',
		name: 'Downward Dog',
		image: 'Downward Dog.jpg'
	},
	{
		zone: 'Yoga Poses',
		name: 'Forward Fold',
		image: 'Forward Fold.jpg'
	},
	{
		zone: 'Yoga Poses',
		name: 'Forward Fold Hands Behind',
		image: 'Forward Fold Hands Behind.jpg'
	},
	{
		zone: 'Yoga Poses',
		name: 'Four Limbs Pose',
		image: 'Four Limbs Pose.jpg'
	}
]

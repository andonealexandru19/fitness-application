export default [
	{
		name: 'Oats with cinnamon and honey',
		quantity: '3 tbsp oats, 1 tsp honey',
		hour: '6:30'
	},
	{
		name: 'Oats and fruits',
		quantity: '3 tbsp oats, 1 tsp honey, 1/2 fruit/ almonds',
		hour: '6:30'
	},
	{
		name: 'Cornflakes',
		quantity: '1 C cornflakes and 1 C milk/ coconut milk',
		hour: '6:30'
	},
	{
		name: 'Fruit milkshake',
		quantity: '3 tbsp low fat yoghurt, 1/2 C milk, 1/2 banana, 5 strawberries, 1 tsp honey',
		hour: '6:30'
	},
	{
		name: 'Smoothy',
		quantity: '1 C milk, 5 strawberries, 1 banana',
		hour: '6:30'
	}
]
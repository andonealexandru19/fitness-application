export default [
	{
		name: 'Fruct',
		quantity: '1 fruct',
		hour: '17:00',
	},
	{
		name: 'Baton de cereale',
		quantity: '1 baton',
		hour: '17:00',
	},
	{
		name: 'Migdale si fruct',
		quantity: '8 migdale, 1 fruct',
		hour: '17:00',
	},
	{
		name: 'Iaurt cu fructe',
		quantity: '150 g iaurt cu fructe, 0% grasime',
		hour: '17:00',
	},
	{
		name: 'Iaurt inghetat cu fructe',
		quantity: '1 C iaurt inghetat si fructe',
		hour: '17:00',
	}
]
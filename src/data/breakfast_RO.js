export default [
	{
		name: 'Ovaz cu scortisoara si miere',
		quantity: '3 linguri ovaz, 1 lingurita miere',
		hour: '6:30'
	},
	{
		name: 'Ovaz si fructe',
		quantity: '3 linguri Ovaz, 1 lingurita miere, 1/2 fruct/ migdale',
		hour: '6:30'
	},
	{
		name: 'Fulgi de porumb',
		quantity: '1 C fulgi de porumb si 1 C lapte/ lapte de cocos',
		hour: '6:30'
	},
	{
		name: 'Milkshake de fructe',
		quantity: '3 linguri iaurt slab, 1/2 C lapte, 1/2 banana, 5 capsuni, 1 lingurita miere',
		hour: '6:30'
	},
	{
		name: 'Smoothy',
		quantity: '1 C lapte, 5 capsuni, 1 banana',
		hour: '6:30'
	}
]
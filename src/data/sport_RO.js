export default [
	{
		zone: 'Partea superioara a trupului',
		name: 'Alternating Push-Up Plank',
		image: 'Alternating Push-Up Plank.jpg'
	},
	{
		zone: 'Partea superioara a trupului',
		name: 'Chest expander',
		image: 'Chest expander.jpg'
	},
	{
		zone: 'Partea superioara a trupului',
		name: 'Diamond Push-Ups',
		image: 'Diamond Push-Ups.jpg'
	},
	{
		zone: 'Partea superioara a trupului',
		name: 'Dive Bomber Push-Ups',
		image: 'Dive Bomber Push-Ups.jpg'
	},
	{
		zone: 'Partea superioara a trupului',
		name: 'Jumping Jacks',
		image: 'Jumping Jacks.jpg'
	},
	{
		zone: 'Partea superioara a trupului',
		name: 'Lying Triceps Lifts', 
		image: 'Lying Triceps Lifts.jpg'
	},
	{
		zone: 'Partea superioara a trupului',
		name: 'One Arm Side Push-up',
		image: 'One Arm Side Push-up.jpg'
	},
	{
		zone: 'Partea superioara a trupului',
		name: 'Overhead Arm Clap',
		image: 'Overhead Arm Clap.jpg'
	},
	{
		zone: 'Partea superioara a trupului',
		name: 'Overhead Press',
		image: 'Overhead Press.jpg'
	},
	{
		zone: 'Partea superioara a trupului',
		name: 'Power Circles',
		image: 'Power Circles.jpg'
	},
	{
		zone: 'Abdomen',
		name: 'Abdominal Crunch',
		image: 'Abdominal Crunch.jpg'
	},
	{
		zone: 'Abdomen',
		name: 'Bent Leg Twist',
		image: 'Bent Leg Twist.jpg'
	},
	{
		zone: 'Abdomen',
		name: 'Burpees',
		image: 'Burpees.jpg'
	},
	{
		zone: 'Abdomen',
		name: 'Elevated Crunches',
		image: 'Elevated Crunches.jpg'
	},
	{
		zone: 'Abdomen',
		name: 'Good Mornings',
		image: 'Good Mornings.jpg'
	},
	{
		zone: 'Abdomen',
		name: 'In and Out Abs',
		image: 'In and Out Abs.jpg'
	},
	{
		zone: 'Abdomen',
		name: 'Inch Worms',
		image: 'Inch Worms.jpg'
	},
	{
		zone: 'Abdomen',
		name: 'Leg Lifts',
		image: 'Leg Lifts.jpg'
	},
	{
		zone: 'Abdomen',
		name: 'Leg Spreaders',
		image: 'Leg Spreaders.jpg'
	},
	{
		zone: 'Abdomen',
		name: 'Mason Twist',
		image: 'Mason Twist.jpg'
	},
	{
		zone: 'Abdomen',
		name: 'Plank',
		image: 'Plank.jpg'
	},
	{
		zone: 'Abdomen',
		name: 'Side Bridge',
		image: 'Side Bridge.jpg'
	},
	{
		zone: 'Partea inferioara a trupului',
		name: 'Calf Raises',
		image: 'Calf Raises.jpg'
	},
	{
		zone: 'Partea inferioara a trupului',
		name: 'Forward Lunges',
		image: 'Forward Lunges.jpg'
	},
	{
		zone: 'Partea inferioara a trupului',
		name: 'Frog Jumps',
		image: 'Frog Jumps.jpg'
	},
	{
		zone: 'Partea inferioara a trupului',
		name: 'Front Kicks',
		image: 'Front Kicks.jpg'
	},
	{
		zone: 'Partea inferioara a trupului',
		name: 'High Jumper',
		image: 'High Jumper.jpg'
	},
	{
		zone: 'Partea inferioara a trupului',
		name: 'Jump Squats',
		image: 'Jump Squats.jpg'
	},
	{
		zone: 'Partea inferioara a trupului',
		name: 'Montain Climber',
		image: 'Montain Climber.jpg'
	},
	{
		zone: 'Partea inferioara a trupului',
		name: 'Rear Lunges',
		image: 'Rear Lunges.jpg'
	},
	{
		zone: 'Partea inferioara a trupului',
		name: 'Running in Place',
		image: 'Running in Place.jpg'
	},
	{
		zone: 'Partea inferioara a trupului',
		name: 'Side Leg Lifts',
		image: 'Side Leg Lifts.jpg'
	},
	{
		zone: 'Partea inferioara a trupului',
		name: 'Side to Side Knee Lifts',
		image: 'Side to Side Knee Lifts.jpg'
	},
	{
		zone: 'Intindere',
		name: 'Abdominal Stretch',
		image: 'Abdominal Stretch.jpg'
	},
	{
		zone: 'Intindere',
		name: 'Ankle on the Knee',
		image: 'Ankle on the Knee.jpg'
	},
	{
		zone: 'Intindere',
		name: 'Arm and Shoulder Stretch',
		image: 'Arm and Shoulder Stretch.jpg'
	},
	{
		zone: 'Intindere',
		name: 'Arm Circles',
		image: 'Arm Circles.jpg'
	},
	{
		zone: 'Intindere',
		name: 'Bend and Reach',
		image: 'Bend and Reach.jpg'
	},
	{
		zone: 'Intindere',
		name: 'Calf Stretch',
		image: 'Calf Stretch.jpg'
	},
	{
		zone: 'Intindere',
		name: 'Chest Stretch',
		image: 'Chest Stretch.jpg'
	},
	{
		zone: 'Intindere',
		name: 'Hamstring Stretch Standing',
		image: 'Hamstring Stretch Standing.jpg'
	},
	{
		zone: 'Intindere',
		name: 'Knee to Chest Stretch',
		image: 'Knee to Chest Stretch.jpg'
	},
	{
		zone: 'Cardio',
		name: 'Butt Kickers',
		image: 'Butt Kickers.jpg'
	},
	{
		zone: 'Cardio',
		name: 'Fast Feet',
		image: 'Fast Feet.jpg'
	},
	{
		zone: 'Cardio',
		name: 'High Knees',
		image: 'High Knees.jpg'
	},
	{
		zone: 'Cardio',
		name: 'Jumping Planks',
		image: 'Jumping Planks.jpg'
	},
	{
		zone: 'Cardio',
		name: 'Lunge Jumps',
		image: 'Lunge Jumps.jpg'
	},
	{
		zone: 'Cardio',
		name: 'Pivoting Upper Cuts',
		image: 'Pivoting Upper Cuts.jpg'
	},
	{
		zone: 'Cardio',
		name: 'Power Jump',
		image: 'Power Jump.jpg'
	},
	{
		zone: 'Cardio',
		name: 'Power Skip',
		image: 'Power Skip.jpg'
	},
	{
		zone: 'Cardio',
		name: 'Shoulder Tap Push-Ups',
		image: 'Shoulder Tap Push-Ups.jpg'
	},
	{
		zone: 'Cardio',
		name: 'Side Hops',
		image: 'Side Hops.jpg'
	},
	{
		zone: 'Cardio',
		name: 'Single Lateral Hops',
		image: 'Single Lateral Hops.jpg'
	},
	{
		zone: 'Pozitii de yoga',
		name: 'Chair Pose',
		image: 'Chair Pose.jpg'
	},
	{
		zone: 'Pozitii de yoga',
		name: 'Cobra Pose',
		image: 'Cobra Pose.jpg'
	},
	{
		zone: 'Pozitii de yoga',
		name: 'Downward Dog',
		image: 'Downward Dog.jpg'
	},
	{
		zone: 'Pozitii de yoga',
		name: 'Forward Fold',
		image: 'Forward Fold.jpg'
	},
	{
		zone: 'Pozitii de yoga',
		name: 'Forward Fold Hands Behind',
		image: 'Forward Fold Hands Behind.jpg'
	},
	{
		zone: 'Pozitii de yoga',
		name: 'Four Limbs Pose',
		image: 'Four Limbs Pose.jpg'
	}
]
export default [
	{
		name: 'Salata si fasole',
		quantity: '1 C fasole rosie si salata',
		hour: '13:30',
		type: '1'
	},
	{
		name: 'Bulgur, Salata si iaurt',
		quantity: '1 C bulgur, 2 linguri iaurt 0% grasime',
		hour: '13:30',
		type: '2'
	},
	{
		name: 'Quinoa si salata cu avocado',
		quantity: 'salata, 1/2 C quinoa, 1/2 avocado',
		hour: '13:30',
		type: '2'
	},
	{
		name: 'Salata si mazare',
		quantity: '1 C mazare si salata',
		hour: '13:30',
		type: '3'
	},
	{
		name: 'Piept de pui cu orez salbatic si salata',
		quantity: '1 piept de pui, 1/2 C orez salbatic, salata',
		hour: '13:30',
		type: '4'
	},
	{
		name: 'Paste fainoase',
		quantity: '1 C Paste fainoase, sos de rosii',
		hour: '13:30',
		type: '5'
	},
	{
		name: 'Somon',
		quantity: '1 Somon fillet, salata',
		hour: '13:30',
		type: '6'
	},
	{
		name: 'Piept de pui si salata',
		quantity: '1 piept de pui, salata',
		hour: '13:30',
		type: '6'
	},
	{
		name: 'Peste alb si legume',
		quantity: '1 peste alb fillet, legume fierte',
		hour: '13:30',
		type: '7'
	}
]
export default [
	{
		name: 'Salata cu ton',
		quantity: 'salata, 1 conserva mica de ton, 1 lingurita de maioneza light, (optional) 1/2 mar verde',
		hour: '20:30',
		type: '1'
	},
	{
		name: 'Salata cu avocado',
		quantity: 'salata, 1/2 avocado',
		hour: '20:30',
		type: '1'
	},
	{
		name: 'Omleta si legume',
		quantity: '2 oua, legume variate, 1 felie de cascaval (12% grasime)',
		hour: '20:30',
		type: '2'
	},
	{
		name: 'Salata cu branza feta',
		quantity: 'salata, 1 bucata medie de branza feta',
		hour: '20:30',
		type: '3'
	},
	{
		name: 'Salata cu piept de pui',
		quantity: '1 piept de pui, salata',
		hour: '20:30',
		type: '3'
	},
	{
		name: 'Salata cu sfecla si oua',
		quantity: '2 oua fierte, 1 sfecla, salata',
		hour: '20:30',
		type: '4'
	},
	{
		name: 'Salata si piept de pui',
		quantity: '1 piept de pui, salata',
		hour: '20:30',
		type: '5'
	},
	{
		name: 'Kebab de pui',
		quantity: '1 pita, 2 linguri iaurt, 2 frigarui de pui',
		hour: '20:30',
		type: '6'
	},
	{
		name: 'Sandvis cu sunca si cascaval',
		quantity: '2 felii de paine, 1 felie sunca, 1 felie cascaval (12% grasime)',
		hour: '20:30',
		type: '7'
	}
]
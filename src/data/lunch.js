export default [
	{
		name: 'Salad and beans',
		quantity: '1 C black eye beans and salad',
		hour: '13:30',
		type: '1'
	},
	{
		name: 'Bulgur, salad and yoghurt',
		quantity: '1 C bulgur, 2 tbsp 0% fat yoghurt',
		hour: '13:30',
		type: '2'
	},
	{
		name: 'Quinoa salad with avocado',
		quantity: 'salad, 1/2 C quinoa, 1/2 avocado',
		hour: '13:30',
		type: '2'
	},
	{
		name: 'Salad and peas',
		quantity: '1 C black eye peas and salad',
		hour: '13:30',
		type: '3'
	},
	{
		name: 'Chicken breast fillet with brown rice and salad',
		quantity: '1 chicken breast fillet, 1/2 C brown rice, salad',
		hour: '13:30',
		type: '4'
	},
	{
		name: 'Pasta',
		quantity: '1 C pasta, tomato sauce',
		hour: '13:30',
		type: '5'
	},
	{
		name: 'Salmon',
		quantity: '1 salmon fillet, salad',
		hour: '13:30',
		type: '6'
	},
	{
		name: 'Chicken and salad',
		quantity: '1 chicken breast fillet, salad',
		hour: '13:30',
		type: '6'
	},
	{
		name: 'White fish and vegetables',
		quantity: '1 white fish fillet, boiled vegetables',
		hour: '13:30',
		type: '7'
	}
]